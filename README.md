# Kde.org website

## Cloning the website

```
mkdir kde-websites && cd kde-websites
git clone --depth 5 git@invent.kde.org:websites/kde-org.git
git clone git@invent.kde.org:websites/aether-sass --branch hugo
cd kde-org
```

This will perform a shallow clone of kde-org.git, reducing the amount of data that needs to be downloaded without usually impacting your workflow. Remove the `--depth 5` part if you want to do a full clone. The aether-sass repo contains the shared theme for many kde website.

## Build websites

You need the latest version of hugo expanded version. It can be found here: https://github.com/gohugoio/hugo/releases. You will also need to have go installed separately.

```
hugo serve
```

## Translations

KDE.org use [hugoI18n](https://invent.kde.org/websites/hugo-i18n) for managing the translations and converting them from and to the gettext format. 

## Licensing

We assume new contributions to the content are licensed under CC-BY-4.0 and to the websites code under LGPL-3.0-or-later unless specified otherwise.
