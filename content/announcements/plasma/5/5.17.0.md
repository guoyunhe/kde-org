---
aliases:
- ../../plasma-5.17.0
changelog: 5.16.5-5.17.0
date: 2019-10-15
layout: plasma
title: 'KDE Plasma 5.17: Thunderbolt, X11 Night Color and Redesigned Settings'
peertube: 5a315252-2790-42b4-8177-94680a1c78fc
figure:
  src: /announcements/plasma/5/5.17.0/plasma-5.17.png
---

{{% i18n_date %}}

Plasma 5.17 is out! Plasma 5.17 is the version where the desktop anticipates your needs. Night Color, the color-grading system that relaxes your eyes when the sun sets, has landed for X11. Your Plasma desktop also recognizes when you are giving a presentation, and stops messages popping up in the middle of your slideshow. If you are using Wayland, Plasma now comes with fractional scaling, which means that you can adjust the size of all your desktop elements, windows, fonts and panels perfectly to your HiDPI monitor.

The best part? All these improvements do not tax your hardware! Plasma 5.17 is as lightweight and thrifty with resources as ever.

{{% i18n_var "Read on for more features, improvements and goodies, or go ahead and <a href='%[1]s'>download Plasma 5.17</a>." "#download" %}}

## Plasma

First of all, Plasma now starts even faster! Among many other optimizations, start-up scripts have been converted from Bash (a slow, interpreted language) to C++ (a fast, compiled language) and now run asynchronously. This means they can run several tasks simultaneously, instead of having to run them one after another. As a result, the time it takes to get from the login screen to the fully loaded desktop has been reduced significantly.

Once your Plasma desktop has loaded, you'll notice changes on the notification front, down in the system tray. One of those changes, as mentioned above, is the ability to avoid interrupting presentations with pop-up notifications. To achieve this, the "Do Not Disturb" mode is automatically enabled when mirroring screens. The Notifications widget has also gained new abilities, and now shows a ringing bell icon when there are new notifications, instead of a circle with a number. This makes the meaning of the icon clearer and its appearance cleaner, as the previous icon tended to get cluttered when the number of notifications went up.

Still in the system tray, you'll discover much better support for public WiFi logins. You will also find that you can set the maximum volume for your audio devices to be lower than 100%.

We improved the widget editing UX, making it easier to move them around and resize them particularly for touch. There's something new in the Task Manager, too. We added a new middle-click behavior to the Task Manager. Middle-clicking on a task of a running app will open a new instance of the app. But if you hover with your cursor on the task and the thumbnail pops-up, middle-clicking on the thumbnail will close that instance of the app.

{{<figure src="/announcements/plasma/5/5.17.0/notification-widget.png" alt="Improved Notifications widget and widget editing UX" caption="Improved Notifications widget and widget editing UX" width="600px" >}}

Both KRunner (Plasma's floating search-and-run widget you can activate with <kbd>Alt</kbd> + <kbd>Space</kbd>) and Kickoff (Plasma's start menu) can now convert fractions of units into other units. This allows you to, for example, type "<i>3/16 inches</i>" into the text box, and KRunner and Kickoff will show "<i>4.7625 mm</i>" as one of the results. Also new is that Kickoff's recent documents section now works with GNOME/GTK apps. The Kickoff tab appearance, which had issues with vertical panels, has now been fixed. As for sticky notes, pasting text into them now strips the formatting by default.

{{<figure src="/announcements/plasma/5/5.17.0/inches-to-cm.png" alt="KRunner now converts fractional units" caption="KRunner now converts fractional units" width="600px" >}}

Your desktop's background is more configurable in Plasma 5.17, as you can now choose the order of the images in the wallpaper <i>Slideshow</i>, rather than it always being random. Another cool feature is a new picture-of-the-day wallpaper source, courtesy of <a href='https://unsplash.com/'>Unsplash</a>. The Unsplash pic of the day option lets you choose the category you want to show, too, so you can pick anything from <i>Design</i>, <i>Christmas</i>, <i>Beach</i>, <i>Cars</i>, <i>Galaxy</i>, and more.

{{<figure src="/announcements/plasma/5/5.17.0/unsplash-pic-of-day.png" alt="<a href='https://unsplash.com/'>Unsplash</a> Picture of the Day" caption="<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day" width="600px" >}}

## System Settings: Thunderbolt, X11 Night Color and Overhauled Interfaces

Modifying the wallpaper is not the only way to change the look of your desktop in Plasma 5.17. The <i>Night Color</i> feature, implemented for Wayland back in Plasma 5.11, is now also available on X11. <i>Night Color</i> subtly changes the hue and brightness of the elements on your screen when it gets dark, diminishing glare and making it more relaxing on your eyes. This feature boasts a modernized and redesigned user interface in Plasma 5.17, and can be manually invoked in the <i>Settings</i> or with a keyboard shortcut.

{{<figure src="/announcements/plasma/5/5.17.0/night-color.png" alt="Night Color settings are now available on X11 too" caption="Night Color settings are now available on X11 too" width="600px" >}}

Speaking of color settings, SDDM's advanced settings tab lets you apply Plasma's color scheme, user's font, icon theme, and other settings to the login screen to ensure it is consistent with the rest of your desktop.

The <i>Settings</i> interface itself has been overhauled in general, and the user interfaces for the <i>Displays</i>, <i>Energy</i>, <i>Activities</i>, <i>Boot Splash</i>, <i>Desktop Effects</i>, <i>Screen Locking</i>, <i>Screen Edges</i>, <i>Touch Screen</i>, and <i>Window Behavior</i> configuration dialogs have all been improved and updated.

We have also added new features to <i>System Settings</i>, like the new panel for managing and configuring Thunderbolt devices. As always, small things also count, and provide a better user experience. An example of that is how we have reorganized and renamed some <i>System Settings</i> dialogs in the <i>Appearance</i> section, and made basic system information available through <i>System Settings</i>.

{{<figure src="/announcements/plasma/5/5.17.0/thunderbolt.png" alt="Thunderbolt device management" caption="Thunderbolt device management" width="600px" >}}

Plasma 5.17 will be even easier to use thanks to the new accessibility feature that lets you move the cursor with the keyboard when using <i>Libinput</i>. To help you save power, we've added a '<i>sleep for a few hours and then hibernate</i>' feature and the option to assign a global keyboard shortcut to turn off the screen.

Improving the look-and-feel of Plasma 5.17 across the board was one of our priorities, and we've made a number of changes to achieve this. For instance, we have made the Breeze GTK theme respect your selected color scheme. Active and inactive tabs in Google Chrome and Chromium now have a visually distinct look, and window borders are turned off by default. We have also given sidebars in settings windows a consistent, modernized appearance.

{{<figure src="/announcements/plasma/5/5.17.0/window-borders.png" alt="Window borders are now turned off by default" caption="Window borders are now turned off by default" width="600px" >}}

## System Monitor

Plasma's System Monitor - KSysGuard - helps you keep an eye on your system, making it easier to catch processes that eat up your CPU cycles or apps that fill up your RAM. In Plasma 5.17, KSysGuard can show CGroup details, allowing you to see container limits. You can also check if a process is hogging your network, as KSysGuard shows network usage statistics for each process.

{{<figure src="/announcements/plasma/5/5.17.0/ksysguard.png" alt="CGroups in System Monitor" caption="CGroups in System Monitor" width="600px" >}}

## Discover

Discover, Plasma's software manager that lets you install, remove and update applications easily, has also been improved. It now includes real progress bars and spinners in various parts of the UI to better indicate progress information. We have also placed icons in the sidebar, and added icons for Snap apps. To help you diagnose potential problems, Discover comes with better 'No connection' error messages in Plasma 5.17.

{{<figure src="/announcements/plasma/5/5.17.0/discover.png" alt="Discover now has icons on the sidebar" caption="Discover now has icons on the sidebar" width="600px" >}}

## KWin: Improved Display Management

One of the main new features of the Plasma 5.17 display manager is that fractional scaling is now supported on Wayland. This means you can scale your desktop to suit your HiDPI monitor perfectly -- no more tiny (or gigantic) fonts and icons on 4K screens! Other features landing in Wayland include the ability to resize GTK headerbar windows from window edges, and improved scrolling behavior (the mouse wheel will always scroll the correct number of lines).

A feature making a come-back in Plasma 5.17 is the ability to close windows in the Present Windows desktop effect with a middle-click. You will also have the option to apply screen settings to the current screen arrangement, or to all screen arrangements.

If you are using the X11 window system, you can now map the Meta (“Super” or “Windows”) and some other key as a shortcut to switch between open windows, instead of <kbd>Alt</kbd> + <kbd>Tab</kbd> which has been the default for years.

## In Memory of Guillermo Amaral

The Plasma 5.17 series is dedicated to our friend Guillermo Amaral. Guillermo was an enthusiastic KDE developer who rightly self described as 'an incredibly handsome multidisciplinary self-taught engineer'. He brought cheer to anyone he met. He lost his battle with cancer last summer but will be remembered as a friend to all he met.

{{<figure src="/announcements/plasma/5/5.17.0/guillermo.png" alt="Guillermo Amaral" caption="Guillermo Amaral" width="600px" >}}
