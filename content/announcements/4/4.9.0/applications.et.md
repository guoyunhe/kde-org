---
title: KDE rakendused 4.9 on viimistletumad ja stabiilsemad
date: "2012-12-05"
hidden: true
---

<p>
KDE teatab rõõmuga paljude populaarsete rakenduste uute versioonide väljalaskmisest. Nende seas on mitu olulist ja kõigile vajalikku tööriista ja rakendust, näiteks Okular, Kopete, KDE PIM, õpirakendused ja mängud.
</p>
<p>
KDE dokumendinäitaja Okular võib nüüd salvestada ja trükkida PDF-dokumentide märkusi ehk annotatsioone. Täiustatud on otsimist, järjehoidjaid ja teksti valimist. Okulari saab seadistada nii, et sülearvuti ei lähe unne ega lülita ekraani välja esitluse ajal, samuti suudab Okular nüüd esitada PDF-failidesse põimitud videoid. Pildinäitaja Gwenview pakub uuena piltide täisekraanirežiimis sirvimise võimalust, rääkimata tervest reast veaparandustest ja väiksematest täiustustest.
</p>
<p>
KDE vaikimisi muusikamängija Juk toetab nüüd last.fm-i koos tagasiside ja kaanepiltide tõmbamisega, samuti on toetatud MPRIS2. Võimalik on ära tunda ja kasutada MP4- ja AAC-failidesse põimitud kaanepilte. Ka KDE videomängija Dragon toetab nüüdsest MPRIS2.
</p>
<p>
Mitmekülgne kiirsuhtlusklient Kopete võib rühmitada kõik kasutajad, keda pole võrgus, ühtsesse rühma ja näidata kontakti oleku muutumist vestlusaknas. Kontaktide nime muutmise võimalust on uuendatud, näidatavat nime saab muuta ka otse.
</p>
<p>
Tõlkerakendusel Lokalize on senisest etem pooleli olevate tõlgete otsing ning paremini korraldatud failiotsingu kaart. Samuti on nüüd võimalik töötada .TS-failidega. Umbrello võib skeemidele automaatselt kujunduse luua ning eksportida graphvizi dot-jooniseid. Okteta võttis kasutusele erinevad vaateprofiilid.
</p>
<h2>Rakendustekogum Kontact</h2>
<p>
Maailma kõige mitmekülgsem PIM-rakenduste kogum Kontact on saanud rohkelt veaparandusi ja ka jõudlus on paranenud. Käesolevas väljalaskes on uuteks võimalusteks importimisnõustaja, mis lubab Thunderbirdist ja Evolutionist tuua KDE PIM-i üle seadistused, kirjad, kalendri ja aadressiraamatu. Uus on ka tööriist, millega saab kirju, seadistusi ja metaandmeid varundada ja taastada. Tagasi on ilmunud KDE 3 aegadest tuntud iseseisv TNEF-manuste näitaja KTnef. KDE PIM võimaldab lõimida ka Google'i ressursse, nii et kasutaja pääseb hõlpsalt ligi oma Google'i kontaktidele ja kalendrile.
</p>
<h2>KDE õpirakendused</h2>

<p>
KDE-Edu tõi välja uue mälu nõudva ja parandava mängu Pairs. Mitmeid täiendusi sai nii õppuritele kui ka õpetajatele mõeldud graafiteooria rakendus Rocs. Algoritme saab nüüd täita samm-sammult, tagasivõtmise ja loobumise võimalused töötavad paremini ning toetatud on ka üksteist katvad graafid. Kstarsis on täiustatud sortimist taevameridiaani ületamise või vaatlusaja järgi ning Digital Sky Survey <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">piltide hankimist</a>.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-pairs.png">
	<img src="/announcements/4/4.9.0/kde49-pairs-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>
</p>
<p>
Marble kiirust optimeeriti ja lisati lõimede kasutamise toetus; lihvi sai ka kasutajaliides. Marble marsruudilaienduste hulgast leiab nüüd OSRM-i (Open Source Routing Machine), toetatud on jalgratta- ja jalakäijamarsruudid ning võrguväline andmemudel, mis lubab hallata marsruute ja otsida andmeid ka siis, kui võrguühendust pole. Samuti võib Marble nüüd näidata õhusõidukite asukohti FlightGeari simulaatori vahendusel.
</p>
<h2>KDE mängud</h2>
<p>
KDE mängud said mitmeid uuendusi. Tublisti lihviti KDE mahjonggimängu Kajongg, millel on nüüd mänguvihjed kohtspikritena, täiustatud mängurobot ja isegi võimalus vestelda, kui mängijad on ühe serveri taga (nüüd on oma server ka kajongg.org-il!). KGoldrunner sai mitu uut taset (Gabriel Miltschitzky panus) ning KPatience suudab salvestamisel meelde jätta mängu ajaloo. Väiksemaid uuendusi sai KSudoku, näiteks paremad vihjed, aga ka seitse uut tasapinnalist ja kolm uut ruumilist mänguvarianti.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai.png">
	<img src="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>

<h4>KDE rakenduste paigaldamine</h4>

<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.9.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.9.0">4.9 infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.9.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.9.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.9.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.9.0#binary">4.9.0 infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.4 või 4.8.0. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.
</p>




<h2>Täna ilmusid veel:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Plasma töötsoonid 4.9 – põhikomponentide täiustused</a></h2>

<p>
Plasma töötsoonide peamistest uuendustest tasub esile tõsta olulisi täiustusi failihalduris Dolphin, X'i terminali emulaatoris Konsole, tegevustes ja aknahalduris Kwin. Neist kõneleb lähemalt <a href="../plasma">'Plasma töötsoonide teadaanne'.</a>
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3"/>KDE platvorm 4.9</a></h2>

<p>
Tänane KDE platvormi väljalase sisaldab veaparandusi, muulaadseid kvaliteediparandusi, võrgutäiustusi ja valmistumist üleminekuks raamistikule 5
</p>
