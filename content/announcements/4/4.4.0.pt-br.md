---
aliases:
- ../4.4
title: KDE SC 4.4.0 Caikaku Release Announcement
date: "2010-02-09"
---

<h3 align="center">
  KDE Software Compilation 4.4.0 introduz a interface para Netbook, alternador de janelas e um framework de autenticação
</h3>

<p align="justify">
  <strong>
    KDE Software Compilation 4.4.0 (Codename: <i>"Caikaku"</i>) Lançado
  </strong>
</p>

<p align="justify">
Hoje a comunidade <a href=http://www.kde.org/>KDE</a> anuncia a disponibilidade imediata do KDE SC 4.4, <i>"Caikaku"</i>, trazendo uma coleção inovadora de aplicações para os usuários de Software Livre. 
Novas tecnológicas foram introduzidas, incluindo redes sociais e ferramentas de colaboração online, uma nova interface orientada a netbooks e inovações infra-estruturais como o framework de autenticação KAuth. De acordo com o sistema de bugs do KDE, 7293 bugs foram corrigidos e 1433 novas requisições de ferramentas foram implementadas.
 A comunidade KDE gostaria de agradecer a todos que tornaram o lançamento dessa versão possível.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="O ambiente de trabalho Plasma">
	</a> <br/>
	<em>O ambiente de trabalho Plasma</em>
</div>
<br/>

<p align=justify>
 Leia o <a href="/announcements/4/4.4.0/guide">Guia Visual</a> para o KDE SC 4.4 para mais detalhes sobre as melhorias no 4.4 ou leia mais para um resumo.
</p>

<h3>
  Ambiente de trabalho Plasma introduz ferramentas sociais e para web
</h3>
<br />
<p align=justify>
 O ambiente de trabalho Plasma oferece as funcionalidades básicas que um usuário necessita para iniciar e gerenciar aplicações, arquivos e configurações globais.
 Os desenvolvedores do ambiente de trabalho KDE entregam-lhe uma nova interface para netbooks, trabalhos artísticos refinados e um melhor fluxo de trabalho para o Plasma.
 A introdução das ferramentas para serviços sociais online traz a natureza cooperativa da comunidade KDE como um time de Software Livre.
</p>

<!-- Plasma Screencast -->
<div class="text-center">
	<em>Interação melhorada com o Plasma </em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">download</a></div>
<br/>

<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> estreia no 4.4.0. Plasma Netbook é uma interface alternativa para o ambiente de trabalho Plasma, especialmente desenhada para um uso ergonômico em netbooks e notebooks menores. 
    O framework Plasma foi desde o início feito com foco nos dispositivos não-desktop também. O Plasma Netbook compartilha diversos componentes com o desktop Plasma, mas é especificamente desenhado para fazer um bom uso do pouco espaço, e para ser também mais recomendado em telas touchscreen.
    O Plasma Netbook também possui um lançador de aplicações em tela cheia e uma interface de pesquisa, além de um catálogo que oferece diversos componentes (widgets) para mostrar conteúdo da web e pequenas ferramentas já conhecidas do Plasma Netbook.
  </li>
  <li>
    A iniciativa do <strong>ambiente de trabalho social</strong> traz mais melhorias para o componente Comunidade (comumente chamado de "Social Desktop widget"),
    permitindo aos usuários enviarem mensagens e encontrarem amigos diretamente do componente. O novo componente "Social News" mostra uma linha do tempo do que está acontecendo na rede social do usuário e o novo componente "Knowledge Base" permite aos usuários procurar por respostas e questões de diferentes provedores incluindo openDesktop.org.
  </li>
  <li>
    A nova funcionalidade do "Alternar janelas" no KWin permite ao usuário <strong>agrupar janelas</strong> em uma interface com abas, permitindo o gerenciamento de um grande número de aplicações facilmente e eficientemente. Mais melhorias no gerenciamento de janelas incluem arrastar janelas para um lado da tela e maximiza-las soltando-as.
    O time do KWin também cooperou com os desenvolvedores do Plasma na melhoria da interação entre as aplicações do ambiente de trabalho trazendo animações mais leves e com performance melhorada.
    Finalmente os artistas podem criar e disponibilizar temas mais facilmente para as janelas graças ao desenvolvimento de um decorador de janelas com a habilidade do uso de gráficos escaláveis.
  </li>
 </ul>
</p>

<!-- Window grouping Screencast -->
<div class="text-center">
	<em>Gerenciamento de Janelas facilitado </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">download</a></div>
<br/>

<h3>
  Aplicações do KDE inovam
</h3>
<p align=justify>
A comunidade KDE oferece um número enorme, porém fácil de usar, de aplicações. Essa versão introduz uma variedade incremental de melhorias e algumas tecnologias inovadoras.
</p>
<p align=justify>
<ul>
  <li>
    Com essa versão do KDE SC a <strong>interface do GetHotNewStuff</strong> foi vastamente melhorada,
    o resultado de um processo de planejamento longo. Esse framework foi criado para permitir  que os contrubuidores externos na comunidade KDE se <strong>conectassem mais facilmente</strong> com os milhões de usuários do seu conteúdo.
    Os usuários podem fazer o download de dados como <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">novos níveis em KAtomic</a>, novas moléculas  ou scripts de melhorias de funcionalidades diretamente na sua aplicação.
    Novas <strong>ferramentas sociais</strong>  como comentários, dar nota a conteúdos, e tornar-se fã de um produto (que mostrarão as atualizações dos usuários desse produto na sua linha do tempo social).
    Os usuários podem <strong>enviar os resultados</strong> da sua própria criatividade para a web de diversas aplicações, eliminando o processo chato de empacotar e enviar manualmente para um website.
  </li>
  <li>
    Dois outros projetos antigos na comunidade KDE amadureceram nessa versão.O Nepomuk, um esforço de pesquisa internacional financiado pela comunidade Européia, atingiu o ponto de estabilidade suficiente e performance.
    <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">A integração da pesquisa no ambiente de trabalho no Dolphin</a> </strong> faz uso do framework semântico Nepomuk para permitir ao usuário na localização e organização dos seus dados.
    A nova linha do tempo mostra os arquivos usados recentemente organizados cronologicamente. Enquanto isso o time do KDE PIM portou suas primeiras aplicações a favor do uso do novo sistema de armazenamento e busca de dados <strong>Akonadi</strong>.
    A aplicação "KDE addressbook" foi reescrita, mostrando uma nova interface em 3 painéis.
    Uma migração mais substancial das aplicações para essas novas tecnologias é planejada para futuras versões do KDE.
  </li>
  <li>
    Além da integração dessas tecnologias, os diversos times de desenvolvedores melhoraram suas aplicações em diversas maneiras. Os desenvolvedores do KGet adicionaram suporte para comparação de assinaturas digitais e download de arquivos a partir de diversas fontes,
    O Gwenview agora inclui uma nova ferramenta para importação de fotos fácil.
    E também, aplicações novas ou totalmente renovadas vêem a luz do dia nessa versão.
    O Palapeli é um jogo puzzle que permite ao usuário resolver quebra-cabeças no seu computador, os usuários também podem criar e compartilhar seus próprios puzzles.
    O cantor é uma interface fácil e intuitiva para software estatístico e científico (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a>, e <a href="http://maxima.sourceforge.net/">Maxima</a>).
    O pacote KDE PIM introduz Blogilo, uma nova aplicação para blogging.
  </li>
</ul>
</p>
<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Procura no Ambiente de Trabalho integrada com o Gerenciador de arquivos do KDE Dolphin">
	</a> <br/>
	<em>Procura no Ambiente de Trabalho integrada com o Gerenciador de arquivos do KDE Dolphin</em>
</div>
<br/>

<h3>
  Plataforma acelera o desenvolvimento
</h3>
<p align=justify>
O forte foco em excelentes tecnologias na comunidade KDE resultou em uma das mais completas, consistentes e eficientes plataformas de desenvolvimento existentes.
 A versão 4.4 introduz diversas novas tecnologias relacionadas à colaboração e redes sociais nas bibliotecas do KDE.
 Nós provemos alternativas livres, poderosas e flexíveis para as tecnologias existentes, ao invés de prender os usuários a somente usar nossos produtos, nós focamos em habilitar inovações em ambos os lados, seja desktop ou online.
<p align=justify>
<ul>
  <li>
    A infraestrutura base do software KDE teve uma enorme gama de atualizações.
    Primeiro de tudo, <strong>Qt 4.6</strong> introduz suporte para a plataforma symbian, além disso um novo framework de animações, multitouch e melhor performance.
    A <strong>tecnologia do Ambiente de Trabalho Social</strong> introduzidas nas últimas versões foram melhoradas com uma identidade de gerenciamento central e introduz libattica como uma biblioteca de acesso ao web service transparente.
    O <strong>Nepomuk</strong> agora usa um backend muito mais estável, fazendo-o a perfeita opção para qualquer metadata, procura ou indexação relacionada nas aplicações.
  </li>
  <li>
    Com KAuth, um novo framework de autenticação foi introduzido.
    O <strong>KAuth provê autenticação segura</strong> e elementos relacionados à interface do usuário para desenvolvedores que desejam executar tarefas com privilégios elevados.
    No Linux, KAuth usa PolicyKit como o seu backend, provendo <strong>integração inter-desktop invisível</strong>.
    O KAuth é atualmente usado em alguns poucos diálogos selecionados no "System Settings" e será integrado no Ambiente de Trabalho Plasma e nas aplicações KDE nos próximos meses.
    O KAuth suporta permissão e revogação de políticas de autorização, armazenamento de senhas e um número de elementos dinâmicos da interface gráfica provendo ajudas visuais (hints) para uso nas aplicações.
  </li>
  <li>
    O <strong>Akonadi</strong>, o cache de groupware Free Desktop vê sua introdução nas aplicações KDE na sua versão 4.4.0.
    A aplicação "livro de endereços" é a pioneira no uso da nova infraestrutura Akonadi liberando seu uso com o KDE SC 4.4.
    O KAddressbook suporta livros de endereços locais como também vários servidores groupware.
    O KDE SC 4.4.0 marca o início da era-Akonadi no KDE SC, com mais aplicações seguindo nas próxima versões.
    Akonadi é pensado no sentido de ser uma interface central para emails, contatos, dados de calendários e outras informações pessoais.
    Ele funciona como um cache transparente para email, servidores groupware e outros recursos online.
  </li>
</ul>
Liberalmente licenciado sob a LGPL (permitindo para desenvolvimento proprietário como livre) e multi-platforma (Linux, UNIX, Mac e MS Windows).
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="A web e as redes sociais no ambiente de trabalho Plasma">
	</a> <br/>
	<em>A web e as redes sociais no ambiente de trabalho Plasma</em>
</div>
<br/>

<h4>
Mais mudanças
</h4>
<p align=justify>
Como mencionado, acima é somente uma seleção de mudanças e melhorias para o ambiente de trabalho KDE, KDE SC, e framework de desenvolvimento de aplicações KDE.
 Uma lista mais compreensiva mas ainda incompleta pode ser encontrada no <a href="http://techbase.kde.org/Schedules/KDE4/4.4_Feature_Plan">Plano de funcionalidades KDE SC 4.4</a> no <a href="http://techbase.kde.org">TechBase</a>.
 Informações sobre aplicações desenvolvidas pela comunidade KDE afora dos pacotes de aplicações KDE podem ser encontradas na <a href="https://www.kde.org/family/">Página da familia KDE</a> e no <a href="http://kde-apps.org">site kde-apps</a>.
</p>

<h4>
Espalhe a notícia e ver o que acontece
</h4>
<p align="justify">
 A Comunidade KDE encoraja todos <strong> para espalhar a notícia </strong> na Net.
Enviar notícias para sites de informação, utilizar canais como o delicious, digg, reddit, twister,
identi.ca. Enviar imagens para serviços como o Facebook, Flickr,
ipernity e Picasa e publicá-las adequadas para grupos. Criar e screencasts
carregá-las para o YouTube, Blip.tv, Vimeo e outros. Não se esqueça de aplicar a tag
no material com o kde <em>tag <strong> kde  </strong> </em>, por isso, é mais fácil para todo mundo encontrar
, e para a equipe do KDE para elaborar relatórios de cobertura para o anúncio do KDE 4.4.

<strong> Ajude-nos a espalhar a notícia, fazer parte dela! </strong> <!-- p-->

</p><p align="justify">
Você pode acompanhar o que está acontecendo em torno do lançamento do KDE 4/4 na Net no
novo site da <a href="http://buzz.kde.org"> <strong> comunidade KDE </strong> </a>. Este site agrega o que acontece em
identi.ca, twister, youtube, flickr, picasaweb, blogs e muitos outros sites de rede social
em tempo real. O livefeed pode ser encontrado em <strong> <a href="http://buzz.kde.org"> buzz.kde.org </a> </strong>.

</p>


<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</div>

Instalando o KDE 4.4.0

</h4>
<p align="justify">
O KDE, incluindo todas as bibliotecas e suas aplicações, está disponível gratuitamente sob licença Open Source. O software do KDE funciona em diversas configurações de hardware, sistemas operacionais e trabalha com qualquer tipo de gerenciador de janelas ou ambiente desktop. Além do Linux e outros sistemas operacionais baseados em UNIX,  você pode encontrar versões do  KDE para o Microsoft Windows no site <a href="http://windows.kde.org"> KDE no Windows </a> e para o Apple Mac OS X no site <a href="http://mac.kde.org/"> KDE no Mac site </a>. São desenvolvidas versões de teste do KDE para várias plataformas móveis como o MS Windows Mobile e Symbian, que podem ser encontradas na web, mas que não tem suporte.

<br>
O KDE pode ser obtido na fonte e em vários formatos binários de <a href="http://download.kde.org/stable/4.4.0/"> http://download.kde.org </a> e pode
também ser obtido em CD-ROM <a href="http://www.kde.org/download/cdrom"> </a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions"> grandes
distribuições Linux</a>.
</p>
<p align="justify">
  <em>Pacotes</em>.
 Alguns distribuidores de Linux têm gentilmente fornecido pacotes binários do KDE 4.4.0
para algumas versões de sua distribuição, e em outros casos os voluntários da comunidade
tem feito o mesmo. <br>

Alguns desses pacotes binários estão disponíveis para download gratuito a partir do site do KDE: <um href="http://download.kde.org/binarydownload.html?url=/stable/4.4.0/"> http://download.kde.org .
Os pacotes binários adicionais, bem como atualizações para os pacotes disponíveis,
estarão disponíveis nas próximas semanas.
</um></p>

<p align="justify">
A maioria dos problemas de desempenho com o driver gráfico binário da <em>NVidia</em> foram  <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA"> resolvidos</a> nas últimas versões disponíveis da NVidia. No entanto, devido a recentes mudanças na <a href="http://x.org"> pilha de gráficos</a> no linux, algumas configurações de hardware e software podem ainda encontrar problemas com o desenho geral velocidade e uma lentidão geral. Entre em contato com o vendedor ou o desenvolvedor do driver da sua distribuição se você se deparar com problemas.

</p>
<p align="justify">
  <a id="package_locations"><em>Localizações dos pacotes</em></a>.
  Para uma lista atual dos pacotes binários disponíveis de que o projeto tem o KDE
sido informado, por favor visite a <a href="/info/4.4.0">página de informação do KDE 4.4.0</a>.
</p>

<h4>
  Compilando o KDE 4.4.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  O código fonte completo para o KDE 4.4.0 pode ser <a href="http://download.kde.org/stable/4.4.0/src/">baixado gratuitamente</a>.

Instruções sobre compilação e instalação do KDE 4.4.0
estão disponíveis na <a href="/info/4.4.0#binary">página de informações do KDE 4.4.0</a>.

</p>
 


