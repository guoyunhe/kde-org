---
aliases:
- ../announce-4.0.4
date: '2008-05-07'
description: KDE Community Ships Fourth Maintenance Update for Fourth Major Version
  for Leading Free Software Desktop.
title: KDE 4.0.4 Release Announcement
---

<h3 align="center">
  KDE Project Ships Fourth Translation and Service Release for Leading Free Software Desktop
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fourth Translation and Service Release of the 4.0
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and 
Translation Updates
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.0.4, the fourth
bugfix and maintenance release for the latest generation of the most advanced and powerful
free desktop. KDE 4.0.4 is the fourth monthly update to <a href="../4.0/">KDE 4.0</a>. It
ships with a basic desktop and many other packages; like administration programs, network tools, 
educational applications, utilities, multimedia software, games, artwork, 
web development tools and more. KDE's award-winning tools and applications are 
available in 49 languages.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>The KDE 4.0 desktop</em>
</div>
<br/>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.0.4/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">
KDE 4.0.4 comes with several bugfixes and performance improvements.
Most of them are recorded in the 
<a href="/announcements/changelogs/changelog4_0_3to4_0_4">changelog</a>.
KDE continues to release updates for the 4.0 desktop on a monthly basis. KDE 4.1, which will
bring <a href="http://techbase.kde.org/index?title=Schedules/KDE4/4.1_Feature_Plan">large 
improvements</a> to the KDE desktop and application will be released in July this year. A
<a href="../4.1-alpha1/">first Alpha</a> is already available for those
that want to take a sneak peak at new features.
<br />
KDE 4.0.4 stabilises the desktop further, users of previous KDE 4.0 versions are
encouraged to update. Improvements revolve around lots of bugfixes and translation updates.
Corrections have been made in such a way that results in only a minimal risk of 
regressions.
<p />

<h4>Extragear</h4>
<p align="justify">
Since KDE 4.0.0, <a href="http://extragear.kde.org">Extragear</a> applications 
are also part of regular KDE releases. 
Extragear applications are KDE applications that are mature, but not part
of one of the other KDE packages.
</p>

<h4>
  Installing KDE 4.0.4 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.4/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.4">KDE 4.0.4 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.0.4
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.0.4 may be <a
href="http://download.kde.org/stable/4.0.4/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.4
  are available from the <a href="/info/4.0.4#binary">KDE 4.0.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
project that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>About KDE 4</h4>
<p align="justify">
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop
shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The
Dolphin file manager, the Okular document reader and the System Settings control
center complete the basic desktop set. 
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>


