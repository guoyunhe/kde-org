---
aliases:
- ../announce-4.1-beta1.de
date: '2008-05-27'
title: KDE 4.1 Beta 1 – Pressemitteilung
---

<h3 align="center">
   Das KDE-Projekt veröffentlicht die erste Beta-Version von KDE 4.1
</h3>

<p align="justify">
  <strong>
Die Gemeinschaft der KDE-Entwickler gibt die Freigabe der ersten Beta-Version von KDE 4.1 bekannt.</strong>
</p>

<p align="justify">
Das <a href="http://www.kde.org/">KDE-Projekt</a> ist stolz die erste Beta-Version von KDE 4.1 zur Veröffentlichung frei zu geben. Diese erste Beta-Version richtet sich an Tester – gleichermaßen an Mitglieder der Entwicklergemeinschaft, sowie KDE-Enthusiasten – um noch vorhandene Fehler und Defizite zu erkennen, so dass die Version 4.1 dann KDE 3 bei den Endbenutzern vollständig ersetzten kann. KDE 4.1 Beta 1 ist in Form von Binärpaketen für eine große Breite von Plattformen, als auch in Form von Quellpaketen, verfügbar. Die endgültige Version von KDE 4.1 wird voraussichtlich im Juli 2008 veröffentlicht werden.
</p>

<h4>
  <a id="changes">Besonderheiten von KDE 4.1 Beta 1</a>
</h4>

<p align="justify">
<ul>
    <li>Die Funktionalität und Möglichkeiten zur Anpassung der Arbeitsoberfläche wurden enorm erweitert.
    </li>
    <li>Die KDE-Programme zum persönlichen Informationsmanagement (PIM) wurden auf KDE 4 portiert.
    </li>
    <li>Zahlreiche neue und neu portierte Anwendungen.
    </li>
</ul>
</p>

<h4>
  Plasma „wird erwachsen“
</h4>
<p align="justify">
Plasma – das innovative System, welches die Menüs, Kontrollleisten und Arbeitsfläche, welche die Arbeitsfläche ausmachen, bereit stellt – wächst rasch. Es unterstützt nun mehrere und in ihrer Größe veränderbare Kontrollleisten, was es den Benutzern ermöglicht ihre Arbeitsoberfläche wieder so flexibel, wie in der Vergangenheit möglich, zu gestalten. Das Menü zum Starten von Anwendungen, Kickoff, wurde umfassend mit einem neuen, klaren Aussehen und weiteren Optimierungen, aufpoliert. Ein überarbeiteter „Programme ausführen“-Dialog erlaubt es versierten Anwendern schnellstmöglich Anwendungen zu starten, Dokumente zu öffnen und Webseiten zu besuchen. Geschwindigkeitsverbesserungen im Bereich des Fenstermanagements bieten sowohl bessere Ergonomie, als auch mehr Blickfänge, wie zum Beispiel die ALT+TAB-Funktion mit „Cover Switch“ oder den mittlerweile obligatorischen „wackelige Fenster“-Effekt.
</p>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-krunner.png">
	<img src="/announcements/4/4.1-beta1/plasma-krunner-small.png" class="img-fluid" alt="Der neue ALT+F2-Dialog von KRunner.">
	</a> <br/>
	<em>Der neue ALT+F2-Dialog von KRunner.</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/plasma-panelcontroller.png">
	<img src="/announcements/4/4.1-beta1/plasma-panelcontroller-small.png" class="img-fluid" alt="Die Kontrollleistenverwaltung kehrt zurück.">
	</a> <br/>
	<em>Die Kontrollleistenverwaltung kehrt zurück.</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-coverswitch.png">
	<img src="/announcements/4/4.1-beta1/kwin-coverswitch-small.png" class="img-fluid" alt="Der „Cover Switch“-Effekt von KWin.">
	</a> <br/>
	<em>Der „Cover Switch“-Effekt von KWin.</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly1.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly1-small.png" class="img-fluid" alt="Fenster, die wackeln.">
	</a> <br/>
	<em>Fenster, die wackeln.</em>
</div>
<br/>
<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kwin-wobbly2.png">
	<img src="/announcements/4/4.1-beta1/kwin-wobbly2-small.png" class="img-fluid" alt="Noch mehr wackelnde Fenster.">
	</a> <br/>
	<em>Noch mehr wackelnde Fenster.</em>
</div>
<br/>
<h4>
  Die Wiederkehr von Kontact
</h4>
<p align="justify">
Kontact – der persönliche Informationsmanager (PIM) von KDE – und die zu ihm gehörenden Anwendungen wurden auf KDE 4 portiert und werden mit KDE 4.1 zum ersten Mal veröffentlicht werden. Viele Funktionen aus dem KDE-3-Entwicklungszweig für Firmen wurden integriert, was Kontact einsatzfähiger in der Geschäftswelt macht. Die Verbesserungen beinhalten neue Komponenten wie zum Beispiel KTimeTracker und die KJots-Komponente zum Erstellen von Notizen, ein neues, stimmiges Erscheinungsbild, bessere Unterstützung für mehrere Kalender und Zeitzonen, sowie eine stabilere Handhabung von E-Mails.
  </p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-calendar.png">
	<img src="/announcements/4/4.1-beta1/kontact-calendar-small.png" class="img-fluid" alt="Mehrere Kalender in Verwendung.">
	</a> <br/>
	<em>Mehrere Kalender in Verwendung.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta1/kontact-kjots.png">
	<img src="/announcements/4/4.1-beta1/kontact-kjots-small.png" class="img-fluid" alt="Notizen in KJots.">
	</a> <br/>
	<em>Notizen in KJots.</em>
</div>
<br/>

<h4>
Die Zahl von KDE 4 Anwendungen steigt
</h4>
<p align="justify">
Verteilt über die Gemeinschaft der KDE-Entwickler wurden zahlreiche Anwendungen auf KDE 4 portiert, oder erfuhren große Verbesserungen in puncto Funktionalität, seit der Veröffentlichung von KDE 4. Dragon Player – der leichtgewichtige Miedienspieler – tritt zum ersten Mal in Erscheinung. Der KDE-CD-Spieler kehrt zurück. Ein neue Anwendung für den Systemabschnitt der Kontrollleiste ermöglicht nicht paralleles Drucken und größere Flexibilität in der freien Arbeitsumgebung. Konqueror erhält eine Sitzungsverwaltung, die Möglichkeit Aktionen im Browser rückgängig zu machen und verbessertes, glatteres Scrollen. Ein neuer Modus zum Durchsehen von Bildern, inklusive einer Vollbild-Oberfläche sind nun in Gwenview enthalten. Dolphin – das Programm zur Dateiverwaltung – unterstützt jetzt Unterfenster und noch viele weitere Funktionen, welche von den KDE-3-Benutzern geschätzt wurden, wie zum Beispiel „Kopieren zu“ und eine verbesserte Baumansicht. Viele Anwendungen, inklusive der Arbeitsoberfläche und den KDE-Lernprogrammen, bieten jetzt neue Inhalte wie zum Beispiel Symbole, Designs, Karten und Unterrichtsmaterialien mittels der Funktion „Neue Sachen abholen“, welche über ein neue, verbesserte Oberfläche verfügt. Netzwerke mittels Zeroconf wurden in vielen Spielen implementiert, was den Benutzern die Qual des Einrichten von Zugangsdaten und dergleichen bei Netzwerkspielen abnimmt.
</p>
<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dolphin-treeview.png">
		<img src="/announcements/4/4.1-beta1/dolphin-treeview-small.png" class="img-fluid" alt="Die Baumansicht in Dolphin.">
		</a> <br/>
		<em>Die Baumansicht in Dolphin.</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/dragonplayer.png">
		<img src="/announcements/4/4.1-beta1/dragonplayer-small.png" class="img-fluid" alt="Dragon Player – Medienspieler.">
		</a> <br/>
		<em>Dragon Player – Medienspieler.</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kdiamond.png">
		<img src="/announcements/4/4.1-beta1/games-kdiamond-small.png" class="img-fluid" alt="KDiamond – Puzzle-Spiel.">
		</a> <br/>
		<em>KDiamond – Puzzle-Spiel.</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/games-kubrick.png">
		<img src="/announcements/4/4.1-beta1/games-kubrick-small.png" class="img-fluid" alt="Die 80er auf Ihrer Arbeitsoberfläche.">
		</a> <br/>
		<em>Die 80er auf Ihrer Arbeitsoberfläche.</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/konqueror.png">
		<img src="/announcements/4/4.1-beta1/konqueror-small.png" class="img-fluid" alt="Konqueror – Webbrowser.">
		</a> <br/>
		<em>Konqueror – Webbrowser.</em>
	</div>
	<br/>
	<div class="text-center">
		<a href="/announcements/4/4.1-beta1/marble-openstreetmap.png">
		<img src="/announcements/4/4.1-beta1/marble-openstreetmap-small.png" class="img-fluid" alt="Marble beim Anzeigen von OpenStreetMaps.">
		</a> <br/>
		<em>Marble beim Anzeigen von OpenStreetMaps.</em>
	</div>
	<br/>

<h4>
Verbesserungen in den gesamten Systemen
</h4>
<p align="justify">
Die Entwickler waren auch damit beschäftigt die KDE-Kern-Bibliotheken und -Infrastruktur zu bereichern. KHTML bekommt einen Geschwindigkeitsschub durch das vorausschauende Laden von Ressoucen, während WebKit – sein Abkömmling – zu Plasma hinzugefügt wurde, um die Verwendung von so genannten „Dashboard Widgets“ von OS X in KDE zu ermöglichen. Die Verwendung der „Widgets on Canvas“-Funktion von Qt 4.4 macht Plasma leichter und stabiler. Die für KDE charakteristische Einfach-Klick-Oberfläche bekommt einen neuen Auswahlmechanismus, der schnelleres Arbeiten und bessere Zugänglichkeit verspricht. Phonon – das plattformübergreifende Multimedia-System – erhält Unterstützung für Untertitel und GStreamer-, DirectShow-9- sowie QuickTime-„Backends“. Die Komponente zur Netzwerkverwaltung wurde um die Unterstützung verschiedener Versionen von NetworkManager erweitert. Unter Beachtung der Tatsache, dass eine freie Arbeitsumgebung Wert auf Vielfalt legt, begannen arbeitsoberflächenunabhängige Bemühungen, wie das Unterstützen der Spezifkationen für Meldungsfenster oder Lesezeichen von freedesktop.org, so dass sich Anwendungen von anderen Arbeitsumgebungen nahtlos in eine KDE-4.1-Sitzung einfügen können.
</p>

<h4>
  Veröffentlichung der endgültigen Version von KDE 4.1
</h4>
<p align="justify">
Die Veröffentlichung von KDE 4.1 ist für den 20. Juli 2008 geplant. Dieser zeitbasierte Termin ist genau sechs Monate nach der Veröffentlichung von KDE 4.0.
</p>

<h4>
  Holen Sie sich’s, führen Sie’s aus, testen Sie’s
</h4>
<p align="justify">
  Freiwillige und Linux- beziehungsweise Unix-Betriebsystemhersteller haben freundlicherweise Binärpakete von KDE 4.0.80 (Beta 1) für die meisten Linux-Distributionen, Mac OS X und Windows, zur Verfügung gestellt. Bitte beachten Sie, dass diese Pakete keinesfalls für den Alltagseinsatz geeignet sind. Werfen Sie einen Blick auf das Softwareverwaltungssystem Ihrer Distribution oder die nachfolgenden Links für distributionsspezifische Anweisungen:</p>

<ul>
<li><a href="http://fedoraproject.org/wiki/Releases/Rawhide"></a>Fedora</li>
<li><em>Debian</em> hat KDE 4.1 Beta 1 in <em>„experimental“</em>.</li>
<li><em>Kubuntu</em>-Pakete sind in Vorbereitung.</li>
<li><a href="http://wiki.mandriva.com/en/2008.1_Notes#Testing_KDE_4">Mandriva</a></li>
<li><a href="http://en.opensuse.org/KDE4#KDE_4_UNSTABLE_Repository_--_Bleeding_Edge">openSUSE</a></li>
<li><a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">Windows</a></li>
<li><a href="http://mac.kde.org/">Mac OS X</a></li>
</ul>

<h4>
  KDE 4.1 Beta 1 (4.0.80) kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  Der vollständige Quelltext von KDE 4.0.80 kann <a
  href="/info/4.0.80">gratis heruntergeladen</a> werden.
Anweisungen zum Kompilieren und Installieren von KDE 4.0.80
  finden Sie auf der <a href="/info/4.0.80">KDE-4.0.80-Informationsseite</a>, oder in der <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">„TechBase“</a>.
</p>

<h4>
  KDE unterstützen
</h4>
<p align="justify">
 KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Softwareprojekt</a>, dass nur existiert und wächst, weil unzählige Freiwillige ihre Zeit und Arbeit investieren. KDE ist immer auf der Suche nach neuen Freiwilligen, die etwas beitragen möchten. Unabhängig davon ob es ums Programmieren, das Beheben oder Berichten von Fehlern, das Schreiben von Dokumentationen, Übersetzungen, Werbung und Öffentlichkeitsarbeit, Geld, usw. geht. Jegliche Beiträge sind willkommen und werden mit Freuden entgegengenommen. Bitte lesen Sie die Seite <a
href="/community/donations/">„KDE unterstützen“</a> für nähere Informationen. </p>

<p align="justify">
Wir würden uns freuen bald von Ihnen zu hören!
</p>

<h4>Über KDE 4</h4>
<p align="justify">
KDE 4.0 ist die innovative, freie Arbeitsumgebung mit zahlreichen Anwendungen, sowohl für den täglichen Gebrauch, als auch für spezielle Zwecke. Plasma ist die neue Benutzerschnittstelle für die Arbeitsoberfläche von KDE 4 und bietet eine intuitive Oberfläche um mit der Arbeitsoberfläche und Anwendungen zu interagieren. Der Konqueror-Webbrowser integriert das Internet in die Arbeitsumgebung. Der Dolphin-Dateimanager, der Okular-Dokumentbetrachter und das „Systemeinstellungen“-Kontrollzentrum komplettieren die Standard-Arbeitsumgebung.
<br />
KDE basiert auf den KDE-Bibliotheken, welche einfachen Zugriff auf Netzwerkressourcen mittels der KIO-Technologie und erweiterte, grafische Effekte durch die Möglichkeiten von Qt4 bieten. Phonon und Solid, welche auch Teil der KDE-Bibliotheken sind, erweitern alle KDE-Anwendungen um ein Multimedia-System und ermöglichen bessere Integration von Hardware.
</p>


