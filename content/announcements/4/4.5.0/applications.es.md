---
title: Las aplicaciones de KDE 4.5.0 mejoran su usabilidad e incluyen rutas de mapas
hidden: true
---

<p>
KDE, una comunidad de Software Libre internacional, se complace en anunciar la disponibilidad inmediata de las aplicaciones de KDE 4.5. Ya se trate de los juegos de alta calidad, el software educativo y de producci&oacute;n o las &uacute;tiles herramientas, todas las aplicaciones han mejorado y se ha facilitado su uso.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/apps-overview.png">
	<img src="/announcements/4/4.5.0/thumbs/apps-overview.png" class="img-fluid" >
	</a> <br/>
	<em>Aplicaciones matem&aacute;ticas, de edici&oacute;n de texto y de entretenimiento</em>
</div>
<br/>

<p>
Las aplicaciones de KDE han mejorado en muchos aspectos. A continuaci&oacute;n se destacan algunos de ellos.
</p>
    <ul>

   <li>El equipo de juegos de KDE ha estado trabajando en nuevas caracter&iacute;sticas y en un nuevo juego: <b>Kajongg</b>, el <a href="http://en.wikipedia.org/wiki/Mahjong">Mahjongg original para 4 jugadores</a>. Dicho juego est&aacute; escrito totalmente en Python, convirti&eacute;ndose en la primera gran aplicaci&oacute;n en Python que forma parte de la familia de juegos de KDE. El juego <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, donde el jugador debe conquistar la galaxia, ahora permite al usuario crear y personalizar sus propios mapas. Tambi&eacute;n se ha facilitado la configuraci&oacute;n del <a href="http://www.kde.org/applications/games/palapeli">juego de puzzles Palapeli</a>, se ha incluido un nuevo conjunto de niveles de KGoldRunner (&quot;Demolition&quot;), y mejorado la integraci&oacute;n con archivos <a href="http://en.wikipedia.org/wiki/Smart_Game_Format">SGF</a> de <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>
    <li>Las <b>aplicaciones educativas</b> tambi&eacute;n se han mejorado mucho. La mayor corresponde al ayudante al aprendizaje <a href="http://www.kde.org/applications/education/parley/">Parley</a>, con una nueva interfaz de pr&aacute;ctica y el soporte a conjugaciones.
    </li>
    <li><a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, la aplicaci&oacute;n de globo terr&aacute;queo virtual, ahora permite planificar rutas y le ofrece la opci&oacute;n de descargar los datos de un mapa antes de salir de viaje para que no necesite conectarse a Internet para utilizar Marble ni sus funciones basadas en OpenStreetMap u OpenRouteService. Puede consultar m&aacute;s informaci&oacute;n sobre <a href="http://nienhueser.de/blog/?p=137">usar las rutas mundiales de Marble mientras est&aacute; desconectado</a>.
    </li>

</ul>

<div class="text-center">
	<a href="/announcements/4/4.5.0/marble-routing.png">
	<img src="/announcements/4/4.5.0/thumbs/marble-routing.png" class="img-fluid" >
	</a> <br/>
	<em>El globo terr&aacute;queo de escritorio Marble ahora tambi&eacute;n se puede utilizar para establecer rutas</em>
</div>
<br/>

<p align="justify">

En Konqueror, las listas de Adblock se pueden actualizar autom&aacute;ticamente. Los usuarios que prefieran utilizar WebKit como motor de renderizado pueden delegar en el KPart de KDE-WebKit. Gwenview ahora se ejecuta con suavidad y sin bloquearse incluso al aplicar efectos de uso intensivo del procesador. Sus operaciones de edici&oacute;n de imagen ahora conservan correctamente todos los metadatos EXIF. Gwenview tambi&eacute;n ha mejorado su usabilidad y aumentado las posibilidades de configuraci&oacute;n. Finalmente, se ha trabajado mucho en KInfoCenter, que ofrece al usuario un resumen de la configuraci&oacute;n del hardware y software de su sistema, mejorando la usabilidad de su ventana principal y la estabilidad.

</p>

<h3>M&aacute;s capturas de pantalla...</h3>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-infocenter.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-infocenter.png" class="img-fluid">
	</a> <br/>
	<em>El apartado visual de KInfoCenter, que muestra informaci&oacute;n sobre su sistema, ha sido modernizado</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/dolphin-metadata.png">
	<img src="/announcements/4/4.5.0/thumbs/dolphin-metadata.png" class="img-fluid">
	</a> <br/>
	<em>Las funciones del Escritorio Sem&aacute;ntico de Dolphin proporcionan metadatos &uacute;tiles</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/gwenview-flickrexport.png">
	<img src="/announcements/4/4.5.0/thumbs/gwenview-flickrexport.png" class="img-fluid">
	</a> <br/>
	<em>Con Gwenview puede compartir de forma sencilla sus im&aacute;genes en la web usando su servicio de im&aacute;genes favorito</em>
</div>
<br/>

<h4>Instalaci&oacute;n de las aplicaciones de KDE</h4>
<p align="justify">
El software de KDE, incluyendo todas sus bibliotecas y aplicaciones, est&aacute; disponible bajo licencias de c&oacute;digo abierto. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Adem&aacute;s de Linux y otros sistemas operativos basados en UNIX, en el sitio web del <a href="http://windows.kde.org">KDE para Windows</a> dispone de versiones para Microsoft Windows de la mayor&iacute;a de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. Tambi&eacute;n puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas m&oacute;viles como MeeGo, MS Windows Mobile y Symbian en la web, pero no est&aacute;n soportadas. 
KDE se puede obtener en c&oacute;digo fuente y varios formatos binarios en <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> y tambi&eacute;n se puede obtener en <a href="http://www.kde.org/download/cdrom">CD-ROM</a> o con cualquiera de las <a href="http://www.kde.org/download/distributions">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de 4.5.0 para algunas versiones de su distribuci&oacute;n, y en otros casos lo han hecho voluntarios de la comunidad. <br />
  Algunos de estos paquetes binarios est&aacute;n disponibles para su descarga gratuita desde <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  En las pr&oacute;ximas semanas estar&aacute;n disponibles paquetes binarios adicionales, as&iacute; como actualizaciones de los ya disponibles.
</p>

<p align="justify">
  <a id="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Equipo de Publicaci&oacute;n de KDE.
</p>

<h4>
  Compilaci&oacute;n de 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  El c&oacute;digo fuente completo de 4.5.0 se puede <a
href="http://download.kde.org/stable/4.5.0/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE 4.5.0 est&aacute;n disponibles en la <a href="/info/4.5.0">p&aacute;gina de informaci&oacute;n de 4.5</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo m&aacute;ximo posible de nuestras publicaciones, le recomendamos encarecidamente el uso de la &uacute;ltima versi&oacute;n de Qt, que a d&iacute;a de hoy es la 4.6.3. Es necesaria para asegurarle una experiencia estable, as&iacute; como algunas mejoras del software KDE que se basan en el framework Qt subyacente.<br />
Algunos controladores gr&aacute;ficos pueden, bajo ciertas condiciones, utilizar XRender en lugar de OpenGL para la composici&oacute;n. Si experimenta una notable lentitud en cuanto al rendimiento gr&aacute;fico, puede que le ayude desactivar los efectos de escritorio, dependiendo del controlador gr&aacute;fico y de la configuraci&oacute;n utilizados. Para poder aprovechar todas las posibilidades del software KDE, le recomendamos que utilice los &uacute;ltimos controladores disponibles para su sistema, puesto que pueden mejorar su experiencia notablemente, tanto en cuesti&oacute;n de funciones opcionales como en rendimiento y estabilidad.
</p>




<h2>Tambi&eacute;n publicados hoy:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="La plataforma de desarrollo de KDE 4.5.0"/>
</a>

<h2>La plataforma de desarrollo de KDE 4.5.0 mejora su rendimiento, estabilidad, introduce una nueva cach&eacute; de alta velocidad y el uso de WebKit</h2>
<p align="justify">
10 de agosto de 2010. Hoy, KDE publica la plataforma de desarrollo de KDE 4.5.0. Esta versi&oacute;n incluye muchas mejoras de rendimiento y estabilidad. La nueva <b>KSharedDataCache</b> est&aacute; optimizada para un acceso r&aacute;pido a los recursos almacenados en disco, como los iconos. La nueva biblioteca <b>KDE WebKit</b> proporciona integraci&oacute;n con las preferencias de red, almacenamiento de contrase&ntilde;as y muchas otras caracter&iacute;sticas de Konqueror. <a href="../platform"><b>Leer el anuncio completo</b></a>
</p>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Los espacios de trabajo de Plasma 4.5.0" />
</a>

<h2>Plasma para escritorio y netbooks 4.5.0: experiencia de usuario mejorada</h2>
<p align="justify">
10 de agosto de 2010. Hoy, KDE publica los espacios de trabajo del escritorio Plasma y Plasma para netbooks 4.5.0. El escritorio Plasma ha refinado mucho su usabilidad. Se ha mejorado la eficiencia de los flujos de gesti&oacute;n de notificaciones y trabajos, el &aacute;rea de notificaciones se ha &quot;limpiado&quot; a nivel visual, y la gesti&oacute;n de entrada de datos entre aplicaciones es m&aacute;s consistente al utilizar el protocolo de notificaciones de Freedesktop.org ya introducido en la versi&oacute;n anterior de Plasma. <a href="../plasma"><b>Leer el anuncio completo</b></a>
</p>
