---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Ships Applications 18.12.1.
layout: application
major_version: '18.12'
title: KDE Ships KDE Applications 18.12.1
version: 18.12.1
---

{{% i18n_date %}}

{{% i18n_var "Today KDE released the first stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../18.12.0" "18.12" %}}

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular, among others.

Improvements include:

- Akregator now works with WebEngine from Qt 5.11 or newer
- Sorting columns in the JuK music player has been fixed
- Konsole renders box-drawing characters correctly again
