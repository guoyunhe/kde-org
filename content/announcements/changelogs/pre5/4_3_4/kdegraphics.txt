------------------------------------------------------------------------
r1045401 | aacid | 2009-11-05 23:25:13 +0000 (Thu, 05 Nov 2009) | 2 lines

Backport aacid * r1045398 okular/trunk/KDE/kdegraphics/okular/ui/ (findbar.cpp findbar.h): stop the search when pressing the [x] button in the find bar

------------------------------------------------------------------------
r1045411 | aacid | 2009-11-06 00:22:41 +0000 (Fri, 06 Nov 2009) | 2 lines

backport aacid * r1045410 okular/trunk/KDE/kdegraphics/okular/core/document.cpp: check the search we are doing is still alive, otherwise doesn't make sense to keep on doing it and even more doing so might make us crash

------------------------------------------------------------------------
r1046877 | gateau | 2009-11-09 21:32:57 +0000 (Mon, 09 Nov 2009) | 1 line

Bumped versions
------------------------------------------------------------------------
r1047675 | aacid | 2009-11-11 19:39:33 +0000 (Wed, 11 Nov 2009) | 8 lines

backport r1047671 okular/trunk/KDE/kdegraphics/okular/core/document.cpp: 
empty the pixmaps queue stack before doing the loop waiting for executing pixmap
requests to empty, otherwise a single shot timed call to sendGeneratorRequest
can kick us in the back cause bad things to happen, ranging from crashes to the
app waiting forever in the loop
Patch provided by pino and tested by me
BUG: 212066

------------------------------------------------------------------------
r1049638 | sars | 2009-11-15 16:03:07 +0000 (Sun, 15 Nov 2009) | 3 lines

Thank you Cobus Carstens for the patch. I applied the patch to the 4.3 branch.

BUG:214679
------------------------------------------------------------------------
r1050640 | aacid | 2009-11-17 19:45:18 +0000 (Tue, 17 Nov 2009) | 4 lines

backport r1050633 okular/trunk/KDE/kdegraphics/okular/core/ (annotations.cpp document.cpp): 
setAttribute with doubles is evil as it uses the current locale and we don't want that, use QString::number
BUGS: 211990

------------------------------------------------------------------------
r1051241 | scripty | 2009-11-19 04:15:44 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1052139 | aacid | 2009-11-20 20:55:04 +0000 (Fri, 20 Nov 2009) | 5 lines

Backport r1052137 | aacid | 2009-11-20 21:52:51 +0100 (Fri, 20 Nov 2009) | 3 lines

Get the viewport before resizing the canvas otherwise the canvas resizal makes the viewport change and later we end up with an incorrect viewport
BUGS: 215463

------------------------------------------------------------------------
r1052935 | sars | 2009-11-22 20:30:53 +0000 (Sun, 22 Nov 2009) | 6 lines

Fix setting preview resolution on scanners that have minimum resolution higher than 50 DPI and have a slider to set the resolution and not combo-boxes.

This commit also decreases the maximum preview resolution to 300 DPI. This to workaround bugs in back-ends that do not return correct values on sane_get_params() before the real scan.

CCBUG:215735

------------------------------------------------------------------------
r1053068 | scripty | 2009-11-23 04:16:44 +0000 (Mon, 23 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1053658 | cfeck | 2009-11-24 14:06:28 +0000 (Tue, 24 Nov 2009) | 6 lines

Call exit() after app destructor (backport r1053200)

BUG: 215671

Thanks for testing, Ryan :)

------------------------------------------------------------------------
r1054510 | pino | 2009-11-26 08:27:09 +0000 (Thu, 26 Nov 2009) | 2 lines

bump version to 0.9.4

------------------------------------------------------------------------
