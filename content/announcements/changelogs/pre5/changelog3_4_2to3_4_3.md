---
aliases:
- ../changelog3_4_2to3_4_3
hidden: true
title: KDE 3.4.2 to KDE 3.4.3 Changelog
---

<p>
This page summaries additions
and corrections that occurred in KDE between the 3.4.2 and 3.4.3 releases.
It has been manually created by maintainers, for an automatically created
complete list of changes click on the 'all SVN changes' links.
</p>

<h3><a name="kdelibs">kdelibs</a> <font size="-2">[<a href="3_4_3/kdelibs.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeaccessibility">kdeaccessibility</a> <font size="-2">[<a href="3_4_3/kdeaccessibility.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeaddons">kdeaddons</a> <font size="-2">[<a href="3_4_3/kdeaddons.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeadmin">kdeadmin</a> <font size="-2">[<a href="3_4_3/kdeadmin.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeartwork">kdeartwork</a> <font size="-2">[<a href="3_4_3/kdeartwork.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdebase">kdebase</a> <font size="-2">[<a href="3_4_3/kdebase.txt">all SVN changes</a>]</font></h3><ul>
	<li>Konqueror: files and dirs with wildcard-like characters are supported again, as well
	wildcards themselves. (<a href="http://bugs.kde.org/show_bug.cgi?id=107170">#107170</a>)</li>
</ul>

<h3><a name="kdebindings">kdebindings</a> <font size="-2">[<a href="3_4_3/kdebindings.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeedu">kdeedu</a> <font size="-2">[<a href="3_4_3/kdeedu.txt">all SVN changes</a>]</font></h3><ul>
	<li>Kig, Kiten, KTouch: Fix incorrect C++ code (<a href="http://bugs.kde.org/show_bug.cgi?id=111452">#111452</a>)</li>
	<li>Kalzium: "Connect Points" does not work in the plottingdialog (<a href="http://bugs.kde.org/show_bug.cgi?id=109980">#109980</a>)</li>
	<li>Kalzium: Correct the data for the orbit of Niobium</li>
	<li>Kig: Fix compile problem</li>
	<li>KmPlot: Fix crash when the Remove action is added in a toolbar and used to remove a graph (<a href="http://bugs.kde.org/show_bug.cgi?id=113387">#113387</a>)</li>
	<li>KStars: Fix crash after telescope not found (<a href="http://bugs.kde.org/show_bug.cgi?id=113611">#113611</a>)</li>
	<li>KVocTrain: Fix langen2kvtml temp file vulnerability (<a href="http://mail.kde.org/pipermail/kde-announce/2005-August/000437.html">More info</a>)</li>
	<li>KWordQuiz: Fix crash in SuSe systems (<a href="http://bugs.kde.org/show_bug.cgi?id=93803">#93803</a>)</li>
</ul>

<h3><a name="kdegames">kdegames</a> <font size="-2">[<a href="3_4_3/kdegames.txt">all SVN changes</a>]</font></h3><ul>
	<li>KMines: Fix log file loading (<a href="http://bugs.kde.org/show_bug.cgi?id=100163">#100163</a>)</li>
	<li>KMines: Fix c++ constness (<a href="http://bugs.kde.org/show_bug.cgi?id=111304">#111304</a>)</li>
	<li>KShinsen: Fix crash on exit (<a href="http://bugs.kde.org/show_bug.cgi?id=94718">#94718</a>)</li>
</ul>

<h3><a name="kdegraphics">kdegraphics</a> <font size="-2">[<a href="3_4_3/kdegraphics.txt">all SVN changes</a>]</font></h3><ul>
	<li>kpdf: Fix a crash some people when login out with kpdf running (<a href="http://bugs.kde.org/show_bug.cgi?id=109764">#109764</a>)</li>
	<li>kpdf: Make back button in konqueror work again</li>
	<li>kpdf: Use the mimetype shell provides us (if any) instead of trying to guess it</li>
	<li>kpdf: Fix a crash in document available at poppler bug 3344 (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=3344">More info</a>)</li>
	<li>kpdf: Disable custom zlib based renderer as was giving problems with some documents (<a href="http://bugs.kde.org/show_bug.cgi?id=110199">#110199</a>)</li>
	<li>kpdf: Fix some problems in document available at poppler bug 3299 (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=3299">More info</a>)</li>
	<li>kpdf: Fix regression that prevented the opening of some malformed files (<a href="http://bugs.kde.org/show_bug.cgi?id=110000">#110000</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=110034">#110034</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=110353">#110353</a>)</li>
	<li>kpdf: Fix some x86_64 problems</li>
	<li>kpdf: Fix a crash in document available at poppler bug 3728 (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=3728">More info</a>)</li>
	<li>kpdf: Fix a crash in document available at poppler bug 3750 (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=3750">More info</a>)</li>
	<li>kpdf: Fix regression that made TOC crash (<a href="http://bugs.kde.org/show_bug.cgi?id=110087">#110087</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=110111">#110111</a>)</li>
	<li>kpdf: Fix page number not displayed when re-opening PDF files (<a href="http://bugs.kde.org/show_bug.cgi?id=110171">#110171</a>)</li>
	<li>kpdf: Fix some painting issues of the minibar buttons (<a href="http://bugs.kde.org/show_bug.cgi?id=112332">#112332</a>)</li>
	<li>kpdf: Fix some small problems with the pageview</li>
	<li>kpdf: Find PS fonts on NetBSD (<a href="http://bugs.kde.org/show_bug.cgi?id=113771">#113771</a>)</li>
    <li>KolourPaint: Prevent accidental drags in the Color Palette from pasting text
        containing the color code</li>
</ul>

<h3><a name="kdemultimedia">kdemultimedia</a> <font size="-2">[<a href="3_4_3/kdemultimedia.txt">all SVN changes</a>]</font></h3><ul>
  <li>JuK: Fix random play not being random between application starts. (<a href="http://bugs.kde.org/show_bug.cgi?id=102238">#102238</a>)</li>
  <li>JuK: Fix Tree View Artists not having artists of different capitalization. (<a href="http://bugs.kde.org/show_bug.cgi?id=102952">#102952</a>)</li>
  <li>JuK: Google cover search now works again (<a href="http://bugs.kde.org/show_bug.cgi?id=112118">#112118</a>)</li>
  <li>JuK: Fix crash on startup after a watched directory was removed. (<a href="http://bugs.kde.org/show_bug.cgi?id=112917">#112917</a>)</li>
  <li>JuK: Fix infinite loop when switching output methods. (<a href="http://bugs.kde.org/show_bug.cgi?id=106038">#106038</a>)</li>
  <li>JuK: Fix 0-width added columns when using manual resizing. (<a href="http://bugs.kde.org/show_bug.cgi?id=101908">#101908</a>)</li>
  <li>JuK: Fix not checking for proxy exceptions for MusicBrainz. (<a href="http://bugs.kde.org/show_bug.cgi?id=107517">#107517</a>)</li>
  <li>JuK: Fix the "Back to Playlist" link in the Now Playing bar to select the playing playlist instead of the Collection List. (<a href="http://bugs.kde.org/show_bug.cgi?id=100572">#100572</a>)</li>
  <li>JuK: Fix the Jump to Playing Item feature to update the Tag Editor. (<a href="http://bugs.kde.org/show_bug.cgi?id=100569">#100569</a>)</li>
  <li>JuK: Fix tabstops being in a seemingly random order in some dialogs.</li>
  <li>JuK: Fix code warning caused by improper signal declaration.</li>
  <li>JuK: A few style guide fixes to clarify some operations.</li>
  <li>JuK: The Now Playing bar is correctly updated when you change the tag of the playing item.</li>
</ul>

<h3><a name="kdenetwork">kdenetwork</a> <font size="-2">[<a href="3_4_3/kdenetwork.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdepim">kdepim</a> <font size="-2">[<a href="3_4_3/kdepim.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdesdk">kdesdk</a> <font size="-2">[<a href="3_4_3/kdesdk.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdetoys">kdetoys</a> <font size="-2">[<a href="3_4_3/kdetoys.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdeutils">kdeutils</a> <font size="-2">[<a href="3_4_3/kdeutils.txt">all SVN changes</a>]</font></h3><ul>
  <li>KCalc: Fix Stefan-Boltzmann constant (<a href="http://bugs.kde.org/show_bug.cgi?id=110532">#110532</a>)</li>
</ul>

<h3><a name="kdevelop">kdevelop</a> <font size="-2">[<a href="3_4_3/kdevelop.txt">all SVN changes</a>]</font></h3><ul>
</ul>

<h3><a name="kdewebdev">kdewebdev</a> <font size="-2">[<a href="3_4_3/kdewebdev.txt">all SVN changes</a>]</font></h3><h4>Quanta Plus</h4>
<ul>
<li>fix crash when deleting a file which is part of a project from the tab context menu (<a href="http://bugs.kde.org/show_bug.cgi?id=111134">#111134</a>)</li>
<li>do not autoclose tag starting with &#060;?, like &#060;?xml</li>
<li>create empty description files for scripts that don't have one</li>
<li>save the content entered in a new file in the VPL editor (<a href="http://bugs.kde.org/show_bug.cgi?id=111278">#111278</a>)</li>
<li>do not allow invocation of the CSS editor in an empty non-CSS document (<a href="http://bugs.kde.org/show_bug.cgi?id=109815">#109815</a>)</li>
<li>fix many cell-merging related errors in the table editor (<a href="http://bugs.kde.org/show_bug.cgi?id=112243">#112243</a>)</li>
<li>fix namespace editing in the attribute editor tree</li>
<li>fix lots of table editor bugs</li>
<li>restore automatic conversion of accented chars behavior to pre-3.4.2</li>
<li>save the content entered in a new file in the VPL editor (<a href="http://bugs.kde.org/show_bug.cgi?id=111278">#111278</a>)</li>
<li>fix loading order of the project view files</li>
<li>replace a leading ~ in an upload profile with the users home folder and avoid a hang</li>
<li>fix lots of VPL related crashes</li>
<li>show DT tags in VPL (<a href="http://bugs.kde.org/show_bug.cgi?id=109723">#109723</a>)</li>
</ul>