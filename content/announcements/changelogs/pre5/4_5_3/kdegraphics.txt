------------------------------------------------------------------------
r1182301 | cgilles | 2010-10-04 19:56:41 +1300 (Mon, 04 Oct 2010) | 2 lines

backport libraw 0.11-beta3 from trunk to KDE 4.5.x branch

------------------------------------------------------------------------
r1182814 | lueck | 2010-10-06 07:21:19 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1185209 | jmueller | 2010-10-13 04:56:53 +1300 (Wed, 13 Oct 2010) | 1 line

Fix compiler warning
------------------------------------------------------------------------
r1185412 | cgilles | 2010-10-13 22:44:46 +1300 (Wed, 13 Oct 2010) | 2 lines

backport commit #1185411 from trunk

------------------------------------------------------------------------
r1185627 | aacid | 2010-10-14 12:37:48 +1300 (Thu, 14 Oct 2010) | 2 lines

make sure the list is shown all the times we show the sidecontainer

------------------------------------------------------------------------
r1185628 | aacid | 2010-10-14 12:38:49 +1300 (Thu, 14 Oct 2010) | 2 lines

no need to look for a new current item if the sidebar is not even shown

------------------------------------------------------------------------
r1185956 | aacid | 2010-10-15 08:20:41 +1300 (Fri, 15 Oct 2010) | 4 lines

rembemer the values of m_menuBarWasShown and m_toolBarWasShown
Patch based in a patch by Victor Blazquez
BUG: 250370

------------------------------------------------------------------------
r1186607 | cgilles | 2010-10-17 18:15:51 +1300 (Sun, 17 Oct 2010) | 2 lines

ipdate libraw to 0.11-beta5

------------------------------------------------------------------------
r1187118 | cgilles | 2010-10-18 23:33:13 +1300 (Mon, 18 Oct 2010) | 2 lines

update internal libraw to 0.11.0-beta7

------------------------------------------------------------------------
r1187349 | cfeck | 2010-10-19 13:25:53 +1300 (Tue, 19 Oct 2010) | 5 lines

Force native graphicssystem (backport r1187348)

BUG: 252256
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1187569 | pino | 2010-10-20 09:15:36 +1300 (Wed, 20 Oct 2010) | 2 lines

support also application/oxps, which is the new mimetype name for XPS documents

------------------------------------------------------------------------
r1189016 | pino | 2010-10-24 07:01:25 +1300 (Sun, 24 Oct 2010) | 5 lines

do not crash when handling links pointing to pages not in the document

BUG: 254610
FIXED-IN: 4.5.3

------------------------------------------------------------------------
