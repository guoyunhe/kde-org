---
aliases:
- /announcements/plasma-5.9.4-5.9.5-changelog
hidden: true
plasma: true
title: Plasma 5.9.5 Complete Changelog
type: fulllog
version: 5.9.5
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Don't make the size of the icon match the size of the text. <a href='https://commits.kde.org/discover/c057f37b6d4d2c8bcdbf33c1be2306dbb7d3e9b6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378570'>#378570</a>
- Make sure categories are populated. <a href='https://commits.kde.org/discover/f122409447ebc8ce9cfd348db81236df1d640a56'>Commit.</a>
- Make sure QSharedPointer is included. <a href='https://commits.kde.org/discover/53702ce2d964843e78c18720a7d97b559bfa8a9c'>Commit.</a>
- Prefer override. <a href='https://commits.kde.org/discover/050713a46113e9929833bc7b8c5dccac77d27d9b'>Commit.</a>
- Remove unneeded include. <a href='https://commits.kde.org/discover/c28481108ffb42ece3cbb0c9dbf254f07f5c1838'>Commit.</a>
- Fix tests. <a href='https://commits.kde.org/discover/4d3dac45ef5651f15721a0cf88c4682b203242e2'>Commit.</a>
- Fix license on the appdata file. <a href='https://commits.kde.org/discover/0c04770a7f2852ca2a2fc8e1af591b076fa6ec46'>Commit.</a>
- Fix crash. <a href='https://commits.kde.org/discover/1efd2873eace2ba23d2a00d717822579ef86fd19'>Commit.</a>

### <a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a>

- Fix query for available modules. <a href='https://commits.kde.org/kde-cli-tools/19a6bb179d5fd5056f34568651bab34e9db390f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378548'>#378548</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5355'>D5355</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Support fractional scailng in KScreen Scaling preview. <a href='https://commits.kde.org/kscreen/94fb089198784d33f227bfdd2b30eeecf2d7f1e4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5412'>D5412</a>
- Set xrdb XFT.DPI value as integer. <a href='https://commits.kde.org/kscreen/c67271c0f207720afd85d92ffaa1eaf4841fd8f4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5487'>D5487</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Sort the themes in decoration KCM. <a href='https://commits.kde.org/kwin/f5a43877a9ea6ddad9eaa8d7498c8ea518c29c81'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5407'>D5407</a>
- Fix KWin decoration KCM showing correct index at startup. <a href='https://commits.kde.org/kwin/3709996f8a884e80139f14ed4c334b1b1c758bf1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5401'>D5401</a>
- [platforms/drm] Explicitly request event context version 2. <a href='https://commits.kde.org/kwin/4ca3d0d94370002430b5131520a11c06b23bdcaa'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5380'>D5380</a>
- Fix crash on dragging titlebar buttons in System Settings. <a href='https://commits.kde.org/kwin/1bfe1164f41dc328d54f7dc6ed13b2fabfde09f6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374153'>#374153</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5117'>D5117</a>
- [kcm_kwindecoration] Respect theme colors in buttons. <a href='https://commits.kde.org/kwin/c3362fe866dd6368855905b8fbc6e828197cb538'>Commit.</a>
- [platforms/drm] Explicitly request event context version 2. <a href='https://commits.kde.org/kwin/c8cd474acd0c364ef37637174afe98578033fa4c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5380'>D5380</a>
- [kcmkwin/compositing] Do not write GLPlatformInterface config. <a href='https://commits.kde.org/kwin/a936516107579df13362f6d2d6f91f26a447d6dd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378114'>#378114</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5285'>D5285</a>
- [slidingpopups] Only remove WindowClosedGrabRole if the effect owns the grab. <a href='https://commits.kde.org/kwin/49fc31059433fe495090b8987e07043b4e3d893d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376609'>#376609</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5225'>D5225</a>
- Fix crash on dragging titlebar buttons in System Settings. <a href='https://commits.kde.org/kwin/c947e0a6012c551f559e45892ac70af58c023b55'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374153'>#374153</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5117'>D5117</a>
- [Aurorae AppMenuButton] Show if client has app menu. <a href='https://commits.kde.org/kwin/c2369507bc6f3fe8ca4f0f70ba684147f6eab022'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5132'>D5132</a>
- Plastik window decoration now supports global menu. <a href='https://commits.kde.org/kwin/3e0ddba683ca68cb50b403cb3893fa2fc9d2d737'>Commit.</a> See bug <a href='https://bugs.kde.org/375862'>#375862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5131'>D5131</a>
- Aurorae window decorations now support global menu button. <a href='https://commits.kde.org/kwin/ffbb25497c3f71c05652617f51e34a1c0a2bbb03'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375862'>#375862</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5130'>D5130</a>
- [kcm_kwindecoration] Respect theme colors in buttons. <a href='https://commits.kde.org/kwin/f0b3be584489d821e27b126a68aa19c5b99749fd'>Commit.</a>
- [kcm_kwindecoration] Respect theme colors in buttons. <a href='https://commits.kde.org/kwin/afcdff25db205e1541c88e300e8ceb8b7239985e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5116'>D5116</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Don't reset the view to the top on gaining focus. <a href='https://commits.kde.org/libksysguard/acb088749af1678814fe7718886ca36209b02e79'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363420'>#363420</a>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- Fix build with Qt 5.9. <a href='https://commits.kde.org/milou/6d9045fea894960b02232bb2ca0a5d63c475d80d'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Update ungrab mouse hack for Qt5.8. <a href='https://commits.kde.org/plasma-desktop/03006fd66ac1c79421a51751fa32dc86bc79bd33'>Commit.</a>
- [Folder View] Fix backport of wrapping fix. <a href='https://commits.kde.org/plasma-desktop/076091e8ee8be54e6c92c1026ff30d87a8286a22'>Commit.</a>
- Backport the word wrapping fix from master. <a href='https://commits.kde.org/plasma-desktop/59528fdc731afe5603bbca9f11c021b6d3415945'>Commit.</a>
- Make sure the "default" sheme is actually default. <a href='https://commits.kde.org/plasma-desktop/fcecf928409c7b9b00f1812b308c1bbba8e93bdc'>Commit.</a>
- Possible to edit the default color scheme. <a href='https://commits.kde.org/plasma-desktop/c8d7f0363a568b6a4c5f0d7061a5858028a0526d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5406'>D5406</a>
- Fix -Wreorder warning. <a href='https://commits.kde.org/plasma-desktop/f7cbf298f9bf430533e184ac1ba218154ecced08'>Commit.</a>
- [Task Manager] Keep entry highlighted when context menu or group dialog is open. <a href='https://commits.kde.org/plasma-desktop/72f5d68657af460cf7e50c7c9ba231c1bbd3d694'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5260'>D5260</a>
- [Folder View] Use toDisplayString which strips passwords. <a href='https://commits.kde.org/plasma-desktop/b9eedbe78c7967b321a4104c93162c89dbbbd647'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5226'>D5226</a>
- [Applet Alternatives] Don't animate highlight resize. <a href='https://commits.kde.org/plasma-desktop/02137a269d7746c47abf4f53c904d7bced6ad017'>Commit.</a>
- [Applet Alternatives] Fix icon size. <a href='https://commits.kde.org/plasma-desktop/e28ea7a47c9669f232b05451e20d001ca61cd2c4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5213'>D5213</a>
- [Task Manager] Don't delay appearance of audio stream in popup. <a href='https://commits.kde.org/plasma-desktop/21ab961c2df528b22377a2f65002ae92425e9cb1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4761'>D4761</a>
- Set wrapMode to NoWrap when maximumLineCount is 1. <a href='https://commits.kde.org/plasma-desktop/a03d08a99e86c13ee50407f5189d70bf0a2b37fc'>Commit.</a>
- Set wrapMode to NoWrap when maximumLineCount is 1. <a href='https://commits.kde.org/plasma-desktop/f5f685ecdc630fdcd767e2a6f611b8f43e7f8c3e'>Commit.</a>
- Add missing member initialization. <a href='https://commits.kde.org/plasma-desktop/0754179a134bb9be149cd7a932188722c0b8f66a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378016'>#378016</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Missing include. <a href='https://commits.kde.org/plasma-nm/e13deb6796a196341f8c1e6e3f310e39801a2b17'>Commit.</a>
- Set auto-negotiate to true for newly created wired connections. <a href='https://commits.kde.org/plasma-nm/47599d8b03e5e7b4978e19acb90da38a01d41aff'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378554'>#378554</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- Switch to newer KDevelop API. <a href='https://commits.kde.org/plasma-sdk/b735dd2b4c0a3c14b61025d091109f8f552dfa20'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5127'>D5127</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Update the ungrab mouse hack for Qt5.8. <a href='https://commits.kde.org/plasma-workspace/75eeae6b42ccf9d553ccc77c3efcf69470540120'>Commit.</a>
- Use KProcess instead of QProcess to launch apps in ksmserver. <a href='https://commits.kde.org/plasma-workspace/c7d21ae2b834bd101d8dba1edaea05edcdb6e8f5'>Commit.</a> See bug <a href='https://bugs.kde.org/369391'>#369391</a>. See bug <a href='https://bugs.kde.org/370528'>#370528</a>
- Launch autostart apps in ksmserver using KRun, not QProcess. <a href='https://commits.kde.org/plasma-workspace/da44dd6eae8b9ef4161680efcb9319a7267fe77e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369391'>#369391</a>. Fixes bug <a href='https://bugs.kde.org/370528'>#370528</a>
- Recognize the WM even if given with a full path. <a href='https://commits.kde.org/plasma-workspace/71908e79626b240d1c32ee88d726c227fa7d6a2d'>Commit.</a> See bug <a href='https://bugs.kde.org/377756'>#377756</a>
- Don't remove other applets in SystemTray::cleanupTask. <a href='https://commits.kde.org/plasma-workspace/4421cc3041b9d172378ba5774241f7079f802073'>Commit.</a> See bug <a href='https://bugs.kde.org/377050'>#377050</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5422'>D5422</a>
- [Media Controller] Enforce PlainText format. <a href='https://commits.kde.org/plasma-workspace/ecbcf6f831020b415325fb36acb2efb957457e75'>Commit.</a>
- [Media Controller] Enforce PlainText format. <a href='https://commits.kde.org/plasma-workspace/4d3760c8aa9d6a99e5239019fb9a29aab2e22896'>Commit.</a>
- Fix off-by-one. <a href='https://commits.kde.org/plasma-workspace/e34b0064d6ff787270127d261be65c3b6780b31f'>Commit.</a> See bug <a href='https://bugs.kde.org/373075'>#373075</a>
- Media Controller can now properly handle and seek long tracks (&gt; 30 minutes). <a href='https://commits.kde.org/plasma-workspace/550860f6366cc99d3f0ff19f74fd3fc3d1bfc0ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377623'>#377623</a>
- When deleting the panel the notifications applet is in, the containment is being destroyed but the corona. <a href='https://commits.kde.org/plasma-workspace/58206408a35a0900b5678231b7aaf490b2a3ec10'>Commit.</a> See bug <a href='https://bugs.kde.org/378508'>#378508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5321'>D5321</a>
- Fix losing wallpaper selection when clicking "Apply". <a href='https://commits.kde.org/plasma-workspace/1a26abe5b0e6af45eab95a6147c627793755e088'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5440'>D5440</a>
- Correctly handle when a new primary screen displaces the old. <a href='https://commits.kde.org/plasma-workspace/ef12ad389ee48208e7a7c8520dc3a05864d2fe87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5323'>D5323</a>
- Fix TasksModel.anyTaskDemandsAttention not updating on window closure. <a href='https://commits.kde.org/plasma-workspace/628593db4c65560ace95759ffca5ce0920de621e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378254'>#378254</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5261'>D5261</a>
- [Windowed Widgets Runner] Fix mimeDataForMatch. <a href='https://commits.kde.org/plasma-workspace/5dff3bc6869a14cd422d2682687b44a53a790ea7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5257'>D5257</a>
- Connect aboutToHide signal from QMenu to relevant libdbusmenu-qt slot. <a href='https://commits.kde.org/plasma-workspace/c24ae6602b0d01cb629e17c4789cecf8c15bd048'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375053'>#375053</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5192'>D5192</a>
- [Calculator Runner] Use "approximate" approximation mode. <a href='https://commits.kde.org/plasma-workspace/34b74d7b5dac9dd458a960e392954ac6c30df3d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/277011'>#277011</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4290'>D4290</a>
- [OSD] Allow disabling OSD through config file. <a href='https://commits.kde.org/plasma-workspace/37ac27c59d97db3bb173ae1786a639cce0c6feb6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D1770'>D1770</a>