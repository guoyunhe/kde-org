---
title: "Distributions with Plasma and KDE Applications"
sassFiles:
- /sass/plasma-desktop.scss
description: 'Choose a Linux distribution and start using KDE applications and Plasma'
layout: distributions
distributions:
  - name: Kubuntu
    logo: /content/distributions/logos/kubuntu.svg
    description: >
      Kubuntu is the Ubuntu version with KDE software by default. Developers strive to provide usability for
      beginners. The default installation includes many popular programs and a utility for managing drivers.

      Canonical, the developer of Ubuntu and Kubuntu, is a KDE Patron.
    link: https://kubuntu.org/
    image: /content/distributions/bg/kubuntu.png
  - name: openSUSE
    logo: /content/distributions/logos/opensuse.svg
    image: /content/distributions/bg/opensuse.png
    link: https://www.opensuse.org
    color: opensuse-section
    description: |
      openSUSE comes in two versions. **Leap**, a stable distribution with regular releases, comes with LTS versions of Linux, Qt and Plasma. **Tumbleweed**, a rolling distribution with the latest versions of all packages

      SUSE is a KDE Patron.
  - name: KDE Neon
    logo: /content/distributions/logos/neon.svg
    image: /content/plasma-desktop/plasma-launcher.png
    link: https://neon.kde.org
    color: neon-section
    description: |
      KDE neon takes the latest Plasma desktop and KDE apps and builds them fresh each day for your
      pleasure, using the stable Ubuntu LTS base.

      Most people will want our User edition built from released software, but we also have Testing
      and Unstable editions built directly from unreleased Git for helping develop our software.
      It is installable as your Linux distro or from Docker images.
  - name: Fedora KDE
    logo: /content/distributions/logos/fedora.svg
    image: /content/distributions/bg/fedora.png
    color: fedora-section
    link: https://spins.fedoraproject.org/kde/
    description: Fedora is a stable distribution sponsored by Red Hat. A new version of Fedora is released every 6 months.
  - name: Manjaro KDE
    logo: /content/distributions/logos/manjaro.svg
    description: Manjaro is a distribution based on Arch Linux, aimed at simplifying its installation and configuration. The Manjaro KDE version, in particular, is configured to use KDE Plasma and KDE core applications.
    color: manjaro-section
    image: /content/distributions/bg/manjaro.png
    link: https://manjaro.org/
subtitle: This page lists some of the most popular Linux distributions with pre-installed KDE software.  We recommend that you familiarize yourself with the descriptions on the project sites in order to get a more complete picture.
---

Install using ROSA Image Writer for:

+ [Windows](http://wiki.rosalab.ru/ru/images/6/62/RosaImageWriter-2.6.2-win.zip)
+ [Linux 32-bit](http://wiki.rosalab.ru/ru/images/4/45/RosaImageWriter-2.6.2-lin-i686.tar.xz)
+ [Linux 64-bit](http://wiki.rosalab.ru/ru/images/7/7f/RosaImageWriter-2.6.2-lin-x86_64.tar.xz)
+ [Mac OS X](http://wiki.rosalab.ru/ru/images/3/33/RosaImageWriter-2.6.2-osx.dmg)

The distribution should also contain installation instructions and a quick-start guide.

You can install KDE applications and Plasma in other Linux distributions and other operating systems. A list of those is available on the [KDE Community wiki](https://community.kde.org/Distributions). You can also [buy devices with Plasma](/hardware).
