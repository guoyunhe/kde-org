---
version: "15.07.80"
title: "KDE Applications 15.07.80 Info Page"
announcement: /announcements/announce-applications-15.08-beta
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
