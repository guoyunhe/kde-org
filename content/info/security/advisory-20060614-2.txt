-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


aRts Security Advisory: artswrapper setuid() return value checking vulnerability
Original Release Date: 2006-06-14
URL: http://www.kde.org/info/security/advisory-20060614-2.txt

0. References

        CVE-2006-2916


1. Systems affected:

	artswrapper, part of aRts, if installed SUID root and running under
        Linux 2.6.0 or newer.

2. Overview:

	artswrapper is a helper application to start the aRts daemon 
	with realtime privileges even for normal users. The wrapper
	assumes that setuid() can not fail for SUID root applications.
        This assertion is wrong under Linux kernel 2.6.0 or newer. 
	Successful exploitation allows a normal user to launch artsd
	as root, which could be exploited to gain system privileges.
       

3. Impact:

	Normal users can become root.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.

	A quick workaround is to remove the suid bit from the 
	artswrapper binary.


5. Patch:

        A patch for aRts 1.2.0 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

	64fcd7bf31d8b0ade22b8f98fbebe0fb  arts-1.2.x.diff

	A patch for aRts 1.0.0 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

	19b351f6bf3055591399edba1b6ccc01  arts-1.0.x-diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFEkEBLvsXr+iuy1UoRAqluAKCGI9LR2b9G467qRYaWwXJH/c0nkwCgrerw
M77/h8+ZDCjrnYcBFAtfbqI=
=N3s7
-----END PGP SIGNATURE-----
