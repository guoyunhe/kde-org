---
title : "KDE 2.0 Info Page"
publishDate: 2000-09-23 00:01:00
unmaintained: true
---

<p>
KDE 2.0 was released on October 23, 2000. Read the
<a href="/announcements/announce-2.0.php">official announcement</a>.</p>

<p>For a high-level overview of the features of KDE, see the
<a href="/info">KDE info page</a></p>

<p>For a graphical tutorial on using KDE 2, see this
<a href="http://www.linux-mandrake.com/en/demos/Tutorial/">tutorial page</a> from Linux Mandrake</p>

<h2>Updates</h2>

Meanwhile, <a href="2.1.php">KDE 2.1</a> has been released. It contains
many new features and a lot of bug fixes.

<p>We strongly encourage to upgrade to 2.1 in case of trouble and before
filing any bug reports. Check the above link for further info including
a change log.</p>

<h2>FAQ</h2>

<h3>The KDE-2.0.1 packages for Mandrake 7.2 give "Can't talk to klauncher"</h3>

Reason:<u>Broken package</u>

<p>kdelibs-2.0.1-1mdk misses several important files. Update
to <a href="http://ftp.sourceforge.net/pub/mirrors/kde/stable/2.0.1/distribution/rpm/Mandrake/7.2/">kdelibs-2.0.1-2mdk</a>.
</p>

<h3>Most KDE applications crash immediately</h3>

Reason:<u>Conflict with libpng version 1.0.8</u> 

<p>Qt 2.2.1 conflicted with libpng 1.0.8. Either downgrade libpng to
version <a href="http://www.libpng.org/pub/png/libpng.html">1.0.7</a>
or update to Qt 2.2.2 which contains a proper fix for this
(and other) problems.</p>

<h3>Most KDE applications crash immediately</h3>

Reason:<u>Environment variables set incorrectly</u> 

<p>The environment variables KDEDIR, KDEHOME, PATH and
LD_LIBRARY_PATH, if set, must point to the correct KDE 2.0 
locations. RedHat users should make sure to change their <i>kde.csh</i> 
and <i>kde.sh</i> scripts.</p>

<h3>Most KDE applications crash immediately</h3>

Reason:<u>Missing ksycoca database</u> 

<p>In some installations the KDE program <i>kbuildsycoca</i> fails
to properly create a file called <i>ksycoca</i>. This file is
needed by KDE to operate properly. Depending on the environment
variable <i>$KDEHOME</i> this file should either be found in
<i>~/.kde/share/config/ksycoca</i> or
<i>~/.kde2/share/config/ksycoca</i>.</p>

<p>Make sure that your <i>$KDEHOME</i> environment variable is
always set to the same value. Leaving it empty is ok, it will then
assume <i>~/.kde</i> in which case you should have a recent
<i>ksycoca</i> in <i>~/.kde/share/config/ksycoca</i>.</p>

<p>Run <i>kbuildsycoca</i> from a command line to generate the
<i>ksycoca</i> file.</p>

<h3>DCOPServer fails to start</h3>

Reason:<u>Too old version of Qt</u> 

<p>You need to have at least Qt version 2.2.1. Version 2.2.0 is
<b>NOT</b> sufficient, no matter what your RPM says. Qt 2.2.2
is recommended, though.</p>

<h3>Logout problems</h3>

Various people have reported that they are not able to log out from
KDE. Others have reported that logging out takes a very long time.
No exact reason for this failure is currently known. 

<h3>Why can't I hear any sound ?</h3>

Stefan Westerfeld has compiled a <a
href="http://www.arts-project.org/doc/handbook/faq.html">
aRts FAQ</a> that will help you going. 

<h3>Why is Konqueror/khtml unable to display some images on the
web?</h3>

Reason:<u>You didn't compile Qt to include support for those
formats</u>
<p>Make sure that you included support for them while compiling
Qt.<br />
</p>

<ul>
<li>for GIF images, include -gif during configuring (if you have a
GIF license).</li>

<li>for JPEG images, make sure that you have jpeglib6b installed
while configuring kdelibs.</li>

<li>for MNG movies, make sure that you compiled Qt with
-system-libmng</li>
</ul>

<h3>Konqueror problems</h3>

See the <a href="http://konqueror.kde.org/faq/">Konqueror
FAQ</a> for Frequently Asked Questions and their answers concerning
KDEs webbrowser and file-manager. 

<h3>KMail and KNode frequently crash (Caldera Open Linux 2.4)</h3>

Reason:<u>A conflict between the mimelib versions distributed with
KDE 1.x and 2.0</u> 

<p>Updated RPMS will be made available soon. Temporary
workaround:</p>

<pre>
cd /opt/kde/lib
rm libmimelib.so
ln -s /usr/lib/libmimelib.so.1.0.1  libmimelib.so
rm libmimelib.so.1
ln -s /usr/lib/libmimelib.so.1.0.1  libmimelib.so.1
</pre>

<h3>Logout and other tasks take very long</h3>

Reason:<u>Problems resolving your hostname</u>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed to the
<a href="http://konqueror.kde.org/faq/">Konqueror FAQ</a> and sound related
questions are answered in the <a href="http://www.arts-project.org/doc/handbook/faq.html">Arts FAQ</a>

<h2>Download and Installation</h2>

See the links listed in the <a
href="/announcements/announce-2.0.php">announcement</a>. The KDE
<a
href="/documentation/faq/install.html">Installation FAQ</a>
provides generic instruction about installation issues.

<p>If you want to compile from sources we offer
<a href="http://developer.kde.org/build/">instructions</a> and help for common
problems in the <a href="http://developer.kde.org/build/compilationfaq.html">Compilation
FAQ</a>.</p>

<h2>Security Issues</h2>

<p><b>NOTE:</b>This section is no longer maintained. Please refer to the
<a href="2.2.2.php">2.2.2 Info page</a> instead.</p>


<h3>Denial of Service</h3>
<p>A continuous stream of nonsense characters directed at the XML-RPC
to DCOP daemon (kxmlrpcd) will cause the daemon to take up 99% of the
CPU for as long as the attack lasts.  This is fixed in 2.0.1</p>

<h2>Bugs</h2>

Here we will provide a list of grave bugs or common pitfalls
surfacing after the release date. 

<p>Please check the bug <a href="http://bugs.kde.org">database</a>
before filing any bug reports. Note that this version has been redeemed
by <a href="2.0.1.php">KDE 2.0.1</a>. Always check the latest stable
version before filing any bug report. This will relieve the developers
from extra work required to deal with outdated reports. Thanks.</p>

<h3>KControl crashes on exit.</h3>

<p>After using a file selection dialog in kcontrol, the style
dialog or the background dialog, KControl will crash when you exit
the program.</p>

<p>Apart from being annoying this crash is harmless.</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 2.0 see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/2.2/kdelibs/KDE2PORTING.html?rev=2.3">
porting guide</a> or discuss your problems with fellow developers
on the kde-de&#x76;&#00101;&#00108;&#64;&#x6b;&#100;e.&#111;rg mailing list. 

<p>There is also info on the <a
href="http://developer.kde.org/documentation/kde2arch.html">architecture</a>
and the programming <a
href="http://developer.kde.org/documentation/library/2.0-api/classref/index.html">
interface</a>.</p>
