---
version: "18.07.80"
title: "KDE Applications 18.07.80 Info Page"
announcement: /announcements/announce-applications-18.08-beta
type: info/application-v1
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
---
