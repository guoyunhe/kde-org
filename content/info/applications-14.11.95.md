---
version: "14.11.95"
title: "KDE Applications 14.11.95 Info Page"
announcement: "/announcements/announce-applications-18.12-beta3"
type: "info/application-v1"
build_instructions: "https://techbase.kde.org/Getting_Started/Build/Historic/KDE4"
---
