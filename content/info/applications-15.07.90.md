---
version: "15.07.90"
title: "KDE Applications 15.07.90 Info Page"
announcement: /announcements/announce-applications-15.08-rc
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---

