---
version: "5.19.90"
title: "KDE Plasma 5.19.90, Beta Release"
type: info/plasma5
signer: Bhushan Shah
signing_fingerprint: FE0784117FBCE11D
---
